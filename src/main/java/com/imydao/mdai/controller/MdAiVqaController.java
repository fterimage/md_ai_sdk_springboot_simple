package com.imydao.mdai.controller;

import com.imydao.ai.api.MdAiAppService;
import com.imydao.ai.api.MdAiVqaService;
import com.imydao.ai.bean.app.AppAuthRequest;
import com.imydao.ai.bean.app.AppAuthRespose;
import com.imydao.ai.common.error.MdAiErrorException;
import com.imydao.spring.boot.autoconfigure.properties.MdAiProperties;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@Slf4j
@Api(tags = "MdAiVqaController")
@RestController
@RequestMapping("/vqa")
public class MdAiVqaController{

    @Autowired
    private MdAiVqaService vqaService;

    @PostMapping("/vpaBlurCheck")
    @ApiOperation(value = "模糊检测", notes = "模糊检测")
    public Boolean vpaBlurCheck(String authToken, MultipartFile multipartFile) throws MdAiErrorException {
        return vqaService.vpaBlurCheck(authToken, multipartFile);
    }


    @PostMapping("/vpaColorCheck")
    @ApiOperation(value = "偏色检测", notes = "偏色检测")
    public Boolean vpaColorCheck(String authToken, MultipartFile multipartFile) throws MdAiErrorException {
        return vqaService.vpaColorCheck(authToken, multipartFile);
    }


    @PostMapping("/vpaBrightnessCheck")
    @ApiOperation(value = "亮度检测", notes = "亮度检测")
    public Boolean vpaBrightnessCheck(String authToken, MultipartFile multipartFile) throws MdAiErrorException {
        return vqaService.vpaBrightnessCheck(authToken, multipartFile);
    }

    @PostMapping("/vpaVideomissingCheck")
    @ApiOperation(value = "黑屏检测", notes = "黑屏检测")
    public Boolean vpaVideomissingCheck(String authToken, MultipartFile multipartFile) throws MdAiErrorException {
        return vqaService.vpaVideomissingCheck(authToken, multipartFile);
    }
}
