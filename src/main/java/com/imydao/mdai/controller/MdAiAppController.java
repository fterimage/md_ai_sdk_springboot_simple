package com.imydao.mdai.controller;

import com.imydao.ai.api.MdAiAppService;
import com.imydao.ai.bean.app.AppAuthRequest;
import com.imydao.ai.bean.app.AppAuthRespose;
import com.imydao.ai.common.error.MdAiErrorException;
import com.imydao.spring.boot.autoconfigure.properties.MdAiProperties;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@Api(tags = "AppController")
@RestController
@RequestMapping("/app")
public class MdAiAppController {

    @Autowired
    private MdAiAppService appService;

    @Autowired
    private MdAiProperties mdAiProperties;

    @GetMapping("/getAccesstoken")
    @ApiOperation(value = "获取token", notes = "获取token")
    public String getAccesstoken() throws MdAiErrorException {
        AppAuthRequest appAuthRequest = new AppAuthRequest();
        appAuthRequest.setAppKey(mdAiProperties.getAppKey());
        appAuthRequest.setAppSecret(mdAiProperties.getAppSecret());

        AppAuthRespose appAuthRespose = appService.appAuth(appAuthRequest);
        log.info("您的访问token为：{}", appAuthRespose.getAccesstoken());
        return appAuthRespose.getAccesstoken();
    }

}
