package com.imydao.mdai.controller;

import com.imydao.ai.api.MdAiAppService;
import com.imydao.ai.api.MdAiVlprService;
import com.imydao.ai.bean.app.AppAuthRequest;
import com.imydao.ai.bean.app.AppAuthRespose;
import com.imydao.ai.bean.vlpr.VlprDetAndRecRespose;
import com.imydao.ai.common.error.MdAiErrorException;
import com.imydao.spring.boot.autoconfigure.properties.MdAiProperties;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@Slf4j
@Api(tags = "MdAiVlprController")
@RestController
@RequestMapping("/vlpr")
public class MdAiVlprController{

    @Autowired
    private MdAiVlprService vlprService;


    @PostMapping("/vlprDetAndRec")
    @ApiOperation(value = "车牌识别", notes = "车牌识别")
    public VlprDetAndRecRespose vlprDetAndRec(String authToken, MultipartFile multipartFile) throws MdAiErrorException {
        return vlprService.vlprDetAndRec(authToken,multipartFile);
    }
}
