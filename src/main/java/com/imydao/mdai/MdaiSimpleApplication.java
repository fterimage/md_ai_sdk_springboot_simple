package com.imydao.mdai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MdaiSimpleApplication {

    public static void main(String[] args) {
        SpringApplication.run(MdaiSimpleApplication.class, args);
    }

}
