package com.imydao.mdai;

// 本示例基于迈道科技机器视觉开发者 MD-AI-SDK-Java

import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.imydao.ai.MdAiClient;
import com.imydao.ai.api.*;
import com.imydao.ai.bean.app.AppAuthRequest;
import com.imydao.ai.bean.app.AppAuthRespose;
import com.imydao.ai.bean.card.IdcdetAndRecRespose;
import com.imydao.ai.bean.facebody.FaceImageFit1VsnRespose;
import com.imydao.ai.common.MdAiBaseResult;
import com.imydao.ai.common.error.MdAiErrorException;
import com.imydao.spring.boot.autoconfigure.properties.MdAiProperties;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


@Slf4j
@Api(tags = "迈道机器视觉")
@RestController
@RequestMapping("/ai")
public class MdAiTestController {

    @Autowired
    private MdAiClient mdAiClient;
    @Autowired
    private MdAiAppService appService;
    @Autowired
    private MdAiCardService cardService;
    @Autowired
    private MdAiFaceBodyService faceBodyService;
    @Autowired
    MdAiVlprService mdAiVlprService;
    @Autowired
    MdAiVqaService mdAiVqaService;


    @Autowired
    private MdAiProperties mdAiProperties;

    //先通过该方法获取token之后，才可以访问其他方法。您可以根据业务要求将token缓存并设置成公共的方法。
    //tips: 也可以通过。https://cv.qhse.cn/swagger/index.html#/App 获取token
    @GetMapping("/getAccesstoken")
    @ApiOperation(value = "获取token", notes = "获取token")
    public String getAccesstoken() throws MdAiErrorException {
        AppAuthRequest appAuthRequest = new AppAuthRequest();
        appAuthRequest.setAppKey(mdAiProperties.getAppKey());
        appAuthRequest.setAppSecret(mdAiProperties.getAppSecret());

        AppAuthRespose appAuthRespose = appService.appAuth(appAuthRequest);
        log.info("您的访问token为：{}", appAuthRespose.getAccesstoken());
        return appAuthRespose.getAccesstoken();
    }

    @PostMapping("/cardService/iDcDetAndRec")
    @ApiOperation(value = "身份证正面识别", notes = "身份证正面识别")
    public IdcdetAndRecRespose iDcDetAndRec(@RequestParam("multipartFile") MultipartFile multipartFile) throws MdAiErrorException {
        IdcdetAndRecRespose idcdetAndRecRespose = cardService.iDcDetAndRec(getAccesstoken(), multipartFile);
        return idcdetAndRecRespose;
    }

    @PostMapping("/faceBodyService/createDb")
    @ApiOperation(value = "创建一个对比库", notes = "创建一个对比库")
    public MdAiBaseResult<Void> createDb(@RequestParam(required = false) String dbName) throws MdAiErrorException {
        if (StrUtil.isBlank(dbName)) {
            dbName = "20230525";
        }
        MdAiBaseResult<Void> result = faceBodyService.createDb(getAccesstoken(), dbName);
        return result;
    }

    @PostMapping("/faceBodyService/faceImageAdd")
    @ApiOperation(value = "增加人脸数据,在对比库中增加样本照片和唯一id", notes = "增加人脸数据,在对比库中增加样本照片和唯一id")
    public MdAiBaseResult<Boolean> faceBodyTest(@RequestParam MultipartFile multipartFile) throws MdAiErrorException {
        //本sdk对于 所有需要file参数的文件，添加了入参为MultipartFile的重写方法。便于spring项目进行调用
        //实现方法为将multipartFile转换为临时文件，进行业务操作后，进行销毁

        String dbName = "20230525";

        //创建一个临时随机id
        String randomedId = RandomUtil.randomString(8);

        //使用multipartFile
        MdAiBaseResult<Boolean> result = faceBodyService.faceImageAdd(getAccesstoken(), randomedId, dbName, multipartFile);

        return result;
    }


    @PostMapping("/faceBodyService/faceImageFit1Vsn")
    @ApiOperation(value = "使用自己的图片去对比，如果匹配回返回使用的id", notes = "使用自己的图片去对比，如果匹配回返回使用的id")
    public FaceImageFit1VsnRespose faceImageFit1Vsn(@RequestParam MultipartFile multipartFile) throws MdAiErrorException {

        //本sdk对于 所有需要file参数的文件，添加了入参为MultipartFile的重写方法。便于spring项目进行调用
        //实现方法为将multipartFile转换为临时文件，进行业务操作后，进行销毁
        String dbName = "20230525";

        //使用multipartFile
        FaceImageFit1VsnRespose result = faceBodyService.faceImageFit1Vsn(getAccesstoken(), dbName, 1, multipartFile);

        //匹配到后为样本图像设置的id
        String label = result.getLabel();

        //大于0.45判断为一个人
        Float confidence = result.getConfidence();

        return result;
    }

    @GetMapping("/getOtherService")
    @ApiOperation(value = "获取其他service", notes = "获取其他service")
    public MdAiClient getOtherService() throws MdAiErrorException {
        //您可以直接使用内置的bean，也可以根据mdaiclent来获取
        MdAiFaceBodyService mdAiFaceBodyService = mdAiClient.getMdAiFaceBodyService();
        MdAiAppService mdAiAppService = mdAiClient.getMdAiAppService();
        MdAiCardService mdAiCardService = mdAiClient.getMdAiCardService();
        MdAiVlprService mdAiVlprService1 = mdAiClient.getMdAiVlprService();
        MdAiVqaService mdAiVqaService1 = mdAiClient.getMdAiVqaService();
        return mdAiClient;
    }
}
